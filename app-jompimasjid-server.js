//
//
//
//


var config				= require('./config');


// HTTP Setup
var express				= require('express');
var app					= express();
var httpBodyParser 		= require('body-parser');
var url 				= require('url');
var fs                  = require('fs');



// email client setup
var nodemailer          = require('nodemailer');
var smtpTransport       = nodemailer.createTransport('SMTP',{
                                service: 'Gmail',
                                auth: {
                                    user: config.smptuser,
                                    pass: config.smtppass
                                }
                            });


// Authentication Setup
var crypto				= require('crypto');
var cryptoiteration 	= config.pbkdf2iterations;
var cryptokeylen 		= config.pbkdf2keylen;
var jwt					= require('jsonwebtoken');

var cors 				= require('cors');
var validator			= require('validator');
var async				= require('async');


// gcloud setup
var gcloud				= require('gcloud');
var datastore			= gcloud.datastore;

// dataset setup
var NAMESPACE 			= 'production';
var dataset				= datastore.dataset({
							projectId: 'jompimasjid-1197',
							keyFilename: 'jompimasjid-ServiceAccount.json'
						});

// storage setup
var gstorage 			= gcloud.storage({
							keyFilename: 'jompimasjid-ServiceAccount.json',
							projectId: 'jompimasjid-1197'
						});
var gbucket 			= gstorage.bucket('jompimasjid-apps');
//var gbucketfile 		= gbucket.file

//var STORAGEIMAGEURL 	= 'https://storage.googleapis.com/apps-jompimasjid';
//var STORAGEIMAGEURLDEFAULTMASJID = 'https://storage.googleapis.com/apps-jompimasjid/images/masjid/no-image.jpg';
//var STORAGEIMAGEURLDEFAULTPROGRAM = 'https://storage.googleapis.com/apps-jompimasjid/images/masjid/no-image.jpg';
var STORAGEIMAGEURL 	= 'https://storage.googleapis.com/jompimasjid-apps';
var STORAGEIMAGEURLDEFAULTMASJID = 'https://storage.googleapis.com/jompimasjid-apps/images/masjid/no-image.jpg';
var STORAGEIMAGEURLDEFAULTPROGRAM = 'https://storage.googleapis.com/jompimasjid-apps/images/masjid/no-image.jpg';
var USERKIND 			= 'User';
var MASJIDKIND	 		= 'Masjid';
var ACTIVITYKIND		= 'Program';






// google map api setup
var geocoder 			= require('geocoder');
var googlemapsapikey 	= {
							key: config.googlemapsjsapikey
						};


// googleapis setup
var google 				= require('googleapis');
var googleapisjsonkey	= require('./jompimasjid-ServiceAccount.json');
var googleapisscopes 	= 	[
								'https://www.googleapis.com/auth/userinfo.email',
								'https://www.googleapis.com/auth/devstorage.full_control',
								'https://www.googleapis.com/auth/cloud-platform',
								'https://www.googleapis.com/auth/datastore'
								
							];
var jwtgoogleapis		= new google.auth.JWT(
									googleapisjsonkey.client_email,
									null,
									googleapisjsonkey.private_key,
									googleapisscopes,
									null
								);

var gdatasetId			= 'jompimasjid-1197';
var gdatastore			= google.datastore({
								version: 'v1beta2',
								auth: jwtgoogleapis,
								projectId: gdatasetId,
								params: {
									datasetId: gdatasetId
								}
							});



var tokenIssuer			= "app-jompimasjid-server";
var tokenSubject		= "app-jompimasjid";
var tokenExpiresIn		= "30d";

var DEFAULT_QUOTASUBSCRIBEDMASJIDLIMIT = 2;



//
// CONFIGURATION
//


var ServerPort 			= process.env.PORT || 8080;

app.set('passwordsecret', config.rahsiaapps);
app.use(httpBodyParser.urlencoded({ extended: false }));
app.use(httpBodyParser.json());
app.use(cors());




function GetJsonWebToken(Username, Email)
{
	return jwt.sign(
		{ 
			iss: tokenIssuer,
			sub: tokenSubject,
			headers: {
				username: Username,
				emailaddress: Email
			} 
		}, 
		app.get('passwordsecret'), 
		{ 
			algorithm: 'HS256',
			expiresIn: tokenExpiresIn
		});
}



function getDistanceFromLatLnInKm(lat1,lon1,lat2,lon2) {
	  var R 		= 6371; 				// Radius of the earth in km
	  var dLat 		= deg2rad(lat2-lat1);  	// deg2rad below
	  var dLon 		= deg2rad(lon2-lon1); 
	  var a 		= Math.sin(dLat/2) * Math.sin(dLat/2) +
					    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
			    		Math.sin(dLon/2) * Math.sin(dLon/2);

	  var c 		= 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	  var d 		= R * c; 				// Distance in km
	  return d;
}

function deg2rad(deg) {
	  return deg * (Math.PI/180)
}





function ISODateString(d)
{
	 function pad(n)
	 {
	 	return n<10 ? '0'+n : n
	 }

	 return d.getUTCFullYear()+'-'
	      + pad(d.getUTCMonth()+1)+'-'
	      + pad(d.getUTCDate())+'T'
	      + pad(d.getUTCHours())+':'
	      + pad(d.getUTCMinutes())+':'
	      + pad(d.getUTCSeconds())+'Z'
}



function getCityStateLocations(Latitude, Longitude, fn)
{
    console.log('Georeverse the location from the Coordination');
	var city = '';
	var state = '';
	var country = '';

	// output
	var result 		= {}; 

	var callback = function(err, data)
	{
		if(err)
		{
            console.log('Failed to GeoReverse');
            console.log('Error: ' + err);
			result = 
			{
				status: false,
				input:
				{
					latitude: Latitude,
					longitude: Longitude
				},
                output: 
                {
              	  city: null,
              	  state: null,
              	  country: null
                }
			}
		}
		else
		{
			for (var i = 0; i < data.results.length; i++) 
			{
				if(data.results[i].types == 'postal_code')
				{
					for (var j = 0; j < data.results[i].address_components.length; j++) 
					{
						if(data.results[i].address_components[j].types.length > 1)
						{
							if(data.results[i].address_components[j].types[1] == 'political')
							{
								if(data.results[i].address_components[j].types[0] == 'locality')
								{
									city = data.results[i].address_components[j].long_name;
								}

								if(data.results[i].address_components[j].types[0] == 'administrative_area_level_1')
								{
									state = data.results[i].address_components[j].long_name;
								}

								if(data.results[i].address_components[j].types[0] == 'country')
								{
									country = data.results[i].address_components[j].long_name;
								}
							}
						}
					};
				}
			};

			result = 
			{
				status: true,
				input:
				{
					latitude: Latitude,
					longitude: Longitude
				},
                data: 
                {
              	  city: city,
              	  state: state,
              	  country: country
                }
			}
		}

		fn(result);
	}

	geocoder.reverseGeocode(
		Latitude, 
		Longitude, 
		callback, 
		googlemapsapikey);
}




function ReadEmailRegisterMasjid(masjidname, masjidid, callback)
{
    console.log('Get the New Masjid Email Registration email raw content');
    
    fs.readFile('./assets/emailregistermasjidraw.html', 'utf8', function (err,data) {
        if (err) 
        {
            return console.log(err);
        }
        
        data = data.replace('@NAMAMASJID@', masjidname);
        data = data.replace('@MASJIDID@', '<' + masjidid + '>');
        
        
        return callback(data);
    });
}



function SendEmailRegisterMasjid(recipient, masjidname, masjidid, callback)
{
    console.log('Sending email of New Masjid Registration');
    
    ReadEmailRegisterMasjid(masjidname, masjidid, function(result){
        var mailoptions = {
            from: 'jom.pi.masjid@gmail.com',
            to: recipient + ', jom.pi.masjid@gmail.com',
            subject: 'Jom Pi Masjid: ' + masjidname,
            html: result
        };

        smtpTransport.sendMail(mailoptions, function(error, response){
            if(error)
                {
                    console.log('mail error');
                    console.log(error);
                    
                    return callback();
                }
            else {
                console.log(response);
                
                return callback();
            }
        });
    });
}

//
//
//
//
//
//
//
//

app.get('/user/uploadphoto/getuploadphotouri', function(req, res){
	console.log('');
	console.log('API - Apps get Resumeable URI to upload a photo');

	// input
	var token 				= url.parse(req.url, true).query.token;
	var group 				= url.parse(req.url, true).query.group;
	var uniqueid 			= '';

	if(group == 'masjid' || group == 'program')
	{
        console.log('Requesting URI to upload image for ' + group);
		uniqueid 			= url.parse(req.url, true).query.uniqueid;
	}
    else{
        console.log('Requesting URI to upload image for User');
    }

	// output
	var result   			= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication expired');
			result = 
			{
				apiname: 'api/app/upload/getuploadphotouri',
                status: true,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			var filename = '';
			var timestamp = new Date().getTime();

			if(group == 'user')
			{
				filename = 'images/' + group +'/' + decoded.headers.username + '_' + timestamp + '.jpeg';
			}
			else
			{
				filename = 'images/' + group +'/' + uniqueid +'_' + timestamp + '.jpeg';
			}

			var gbucketfile = gbucket.file(filename);
			gbucketfile.createResumableUpload(function(err, uri){
				if(err)
				{
                    console.log('Error during creating URI to upload photo');
					result = 
					{
						apiname: 'api/app/upload/getuploadphotouri',
		                status: false,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					result = 
					{
						apiname: 'api/app/upload/getuploadphotouri',
		                status: true,
		                parameters: 
		                {
		                	isAuth: true,
		                    data: {
		                    	uri: uri,
		                    	name: filename
		                    }
		                }
					}

					res.send(result);
				}
			});
		}
	});
});




app.post('/user/uploadphoto/uploadcompleted', function(req, res){
	console.log('');
	console.log('API - Upload photo completed');

	// input
	var token 			= req.body.token;
	var filename 		= req.body.filename;


	// output
	var result 			= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/session',
                status: true,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			var gbucketfile = gbucket.file(filename);
			gbucketfile.makePublic(function(err, apiResponse){
				if(err)
				{
                    console.log('Error making the uploaded photo public');
					result = 
					{
						apiname: 'api/user/session',
		                status: false,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					result = 
					{
						apiname: 'api/user/session',
		                status: true,
		                parameters: 
		                {
		                	isAuth: true,
		                    imageurl: STORAGEIMAGEURL + '/' + filename
		                }
					}

					res.send(result);
				}
			});
		}
	});
});






app.get('/user/session', function(req, res){
	console.log('');
	console.log('API - User Session Check');


	// input
	var token 					= url.parse(req.url, true).query.token;

	// output
	var result 					= {};

	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/session',
                status: true,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			result = 
			{
				apiname: 'api/user/session',
                status: true,
                parameters: 
                {
                	isAuth: true,
                    message: 'Authenticated'
                }
			}

			res.send(result);
		}
	});
});









//
// USER API
//
app.post('/user/authentication', function(req, res)
	{
		console.log('');
		console.log('API - User Authentication');


		// input
		var login 				= req.body.username.toLowerCase();
		var passkey 			= req.body.passkey;

		// output
		var result 				= {};

		// check how user trying to login
		// var loginType = (validator.isEmail(login) ? 'emailNormalized =' : 'username =');
		
		var getUserEmailQuery = dataset.createQuery(NAMESPACE, USERKIND)
			 .filter('emailNormalized =', login);

		dataset.runQuery(getUserEmailQuery, function(err, entities)
			{
				if(err)
				{
                    console.log('Error in getting the user from the database');
					result = {
						apiname: 'api/user/authentication',
			            status: false,
			            parameters: 
			            {
			            	message: err,
			                data: 
			                {
				              	isAuth: false,
				              	token: 'null'
			                }
			            }
			        }

				 	res.send(result);
				}
				else
				{
					if(entities.length == 0)
					{
						result = {
							apiname: 'api/user/authentication',
				            status: false,
				            parameters: 
				            {
				            	message: 'User does not exists',
				              data: {
				              	isAuth: false,
				              	token: 'null'
				              }
				            }
				        }

				        res.send(result);
					}
					else
					{
						if(entities[0].data.credential == undefined || entities[0].data.credential.asamgaram == undefined)
						{
							result = {
								apiname: 'api/user/authentication',
					            status: true,
					            parameters: 
					            {
					              data: {
					              	isAuth: false,
					              	token: 'null'
					              }
					            }
					        }
					        res.send(result);
						}
						else
						{
                            console.log('Authenticating ...');
							var challengeSalt = new Buffer(entities[0].data.credential.asamgaram, 'hex');
							var challengehashedpassword = crypto.pbkdf2Sync(
															passkey,
															challengeSalt,
															cryptoiteration,
															cryptokeylen);


							if(entities[0].data.credential.passphrase == challengehashedpassword.toString('hex'))
							{
                                console.log('Authentication success');
								result = {
									apiname: 'api/user/authentication',
						            status: true,
						            parameters: 
						            {
						              data: {
						              	isAuth: true,
						              	token: GetJsonWebToken(entities[0].data.usernameNormalized, entities[0].data.emailNormalized)
						              }
						            }
						        }

						        res.send(result);
							}
							else
							{
                                console.log('Authentication failed');
								result = {
									apiname: 'api/user/authentication',
						            status: true,
						            parameters: 
						            {
						              data: {
						              	isAuth: false,
						              	token: 'null'
						              }
						            }
						        }
						        res.send(result);
							}
						}
					}
				}
			});
	});








// Saiful - 19/2/16 Removed this function since not in used
//app.get('/user/currentlocation', function(req, res)
//	{
//		console.log('');
//		console.log('API - Get user current location');
//		console.log('Request from ' + req.url);
//
//		// input
//		var longitude 			= url.parse(req.url, true).query.longitude;
//		var latitude 			= url.parse(req.url, true).query.latitude;
//
//		// output
//		var result1 				= {};
//
//		getCityStateLocations(latitude,longitude, function(result){
//			res.send(result);
//		});
//
//		
//
//	});








// check if the user already signed up
app.get('/user/checkifsignedup', function(req, res)
	{
		console.log('');
		console.log('API - Check User Signed Up');


		// input
		var userEmail 		= url.parse(req.url, true).query.emailaddress.toLowerCase();

		// output
		var result 			= {};

		console.log(userEmail);


		var userKey = dataset.key({
            namespace: NAMESPACE, 
            path: [USERKIND, userEmail]
        });
		dataset.get(userKey, function(err, entity)
			{
				if(err)
				{
                    console.log('Error looking for user in database');
					console.log(err);
					result = 
					{
						apiname: 'api/user/signedup',
		                status: false,
		                parameters: 
		                {
		                	isUserSignedUp: false,
		                  	data: err
		                }
					}

					res.send(result);
				}
				else
				{
					// even if th user is not existed.
					if(entity == undefined)
					{
                        console.log('User in not existed');
						result = 
						{
							apiname: 'api/user/signedup',
			                status: true,
			                parameters: 
			                {
			                  data: {
			                  	isUserSignedUp: false,
			                  	message: 'NOTEXISTS'
			                  }
			                }
						}
					}
					else
					{
                        console.log('User found');
						result = 
						{
							apiname: 'api/user/signedup',
			                status: true,
			                parameters: 
			                {
			                  data: {
			                  	isUserSignedUp: true,
			                  	message: 'EXISTS'
			                  }
			                }
						}
					}
					res.send(result);
				}
			});
	});








// register new user
app.post('/user/newregistration', function(req, res)
	{
		console.log('');
		console.log('API - Register New User');


		// input
		var username 			= req.body.email.toLowerCase();
		var email 				= req.body.email.toLowerCase();
		var passphrase 			= req.body.passkey;
		var firstname 			= req.body.firstname.toLowerCase();
		var lastname 			= req.body.lastname.toLowerCase();
		var gender 				= req.body.gender;
		var latitude 			= req.body.latitude;
		var longitude 			= req.body.longitude; 



		// output
		var result 				= {};


		// input data processing
		var datecreated 		= new Date();
		var datemodified 		= new Date();

		var isLocationProvided 	= false;
		if(latitude != null && longitude != null)
			isLocationProvided = true;

		var avatarUrl 			= '';
		if(gender.toUpperCase() == 'MALE')
			avatarUrl = STORAGEIMAGEURL + '/images/user/USER_DEFAULT_MALE.png';
		else
			avatarUrl = STORAGEIMAGEURL + '/images/user/USER_DEFAULT_FEMALE.png';


        console.log('Salting Password');
		var salt = crypto.randomBytes(256);
		var salthex = salt.toString('hex');
		var hashedpasskey = crypto.pbkdf2Sync(passphrase, salt, cryptoiteration, cryptokeylen).toString('hex');


		var credential = {
			username: username,
			email: email,
			passphrase: hashedpasskey,
			asamgaram: salthex,
			confirmregistration: false
		}

		var newUser = dataset.key({namespace: NAMESPACE, path: [USERKIND, email]});
		dataset.get(newUser, function(err, entity){
			if(err)
			{
                console.log('Error in database when trying to check the user existence');
				result = 
				{
					apiname: 'api/user/newregistraton',
	                status: false,
	                parameters: 
	                {
	                  message: {
	                  	error: 0,
	                  	data: err
	                  }
	                }
				}

				res.send(result);
			}
			else
			{
				if(entity != undefined)
				{
					console.log('User with similar username is already existed');
					result = 
					{
						apiname: 'api/user/newregistraton',
		                status: false,
		                parameters: 
		                {
		                  message: {
		                  	error: 1,
		                  	data: "User already exists."
		                  }
		                }
					}

					res.send(result);
				}
				else
				{
					if(isLocationProvided)
					{
                        console.log('Registration with location provided');
						getCityStateLocations(latitude, longitude, function(location){

							var newUserData = {
								emailNormalized: 		email,
								usernameNormalized: 	username,
								firstname: 				firstname,
								lastname: 				lastname,
								gender: 				gender,
								isGuru: 				false,
								imageUrl: 				{
															avatar: avatarUrl
														},
								subscribedmasjids: 		[],
								datecreated: 			datecreated,
								datemodified: 			datemodified,
								credential: 			credential, 
								location: 				location,
								location_city: 			location.data.city,
								location_state: 		location.data.state,
								location_country: 		location.data.country,
								purchasedfeatures: 		{
															createnewmasjid: false,
															quotasubscribedmasjidlimit: DEFAULT_QUOTASUBSCRIBEDMASJIDLIMIT
														}
							};

							dataset.save({
								key: newUser,
								data: newUserData
							}, function(err)
							{
								if(err)
								{
									result = 
									{
										apiname: 'api/user/newregistraton',
						                status: false,
						                parameters: 
						                {
						                  message: {
						                  	error: 2,
						                  	data: err
						                  }
						                }
									}

									res.send(result);
								}
								else
								{
									// send back result
									result = 
									{
										apiname: 'api/user/newregistration',
						                status: true,
						                parameters: 
						                {
						                  message: GetJsonWebToken(username, email)
						                }
									}

									res.send(result);
								}
							});
						});
					}
					else
					{
						var noLocation = 
						{
						  status: false,
						  input: 
						  {
						    latitude: 'null',
						    longitude: 'null'
						  },
						  data: 
						  {
						    city: 'null',
						    state: 'null',
						    country: 'null'
						  }
						};

						var newUserData = {
							emailNormalized: 			email,
							usernameNormalized: 		username,
							firstname: 					firstname,
							lastname: 					lastname,
							gender: 					gender,
							isGuru: 					false,
							imageUrl: 					{
															avatar: avatarUrl
														},
							subscribedmasjids: 			[],
							datecreated: 				datecreated,
							datemodified: 				datemodified,
							credential: 				credential, 
							location: 					noLocation,
							location_city: 				'null',
							location_state: 			'null',
							location_country: 			'null',
							purchasedfeatures: 			{
															createnewmasjid: false,
															quotasubscribedmasjidlimit: DEFAULT_QUOTASUBSCRIBEDMASJIDLIMIT
														}
						};

						dataset.save({
							key: newUser,
							data: newUserData
						}, function(err)
						{
							if(err)
							{
                                console.log('Error in user registration');
								result = 
								{
									apiname: 'api/user/newregistraton',
					                status: false,
					                parameters: 
					                {
					                  message: {
					                  	error: 2,
					                  	data: err
					                  }
					                }
								}

								res.send(result);
							}
							else
							{
								console.log('Registration success');
								result = 
								{
									apiname: 'api/user/newregistration',
					                status: true,
					                parameters: 
					                {
					                  message: GetJsonWebToken(username, email)
					                }
								}

								res.send(result);
							}
						});
					}
				}
			}
		});
	});






app.get('/user/getuserinfo', function(req, res){
	console.log('');
	console.log('API - Get User Info');


	// input
	var token 					= url.parse(req.url, true).query.token;
	var infotype			 	= url.parse(req.url, true).query.infotype;


	// output
	var result 					= {};






	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/getuserinfo',
                status: true,
                parameters: 
                {
                	isAuth: false,
                    message: err,
                    data: 'null'
                }
			}

			res.send(result);
		}
		else
		{
			// check input
			if(infotype == undefined)
			{
				result = 
				{
					apiname: 'api/user/getuserinfo',
		            status: false,
		            parameters: 
		            {
		            	isAuth: true,
		                message: 'Info Type is not supported',
		                data: 'null'
		            }
				}

				res.send(result);
			}
			else if(infotype.toUpperCase() != 'SUMMARY' 				&& 
						infotype.toUpperCase() != 'PURCHASEDMASJID'
					)
			{
				result = 
				{
					apiname: 'api/user/getuserinfo',
		            status: false,
		            parameters: 
		            {
		            	isAuth: true,
		                message: 'Info Type is not supported',
		                data: 'null'
		            }
				}

				res.send(result);
			}
			else
			{
				var userPath = dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});
				dataset.get(userPath, function(err, entity){
					if(err)
					{
                        console.log('Server error in getting the User entity');
						result = 
						{
							apiname: 'api/user/getuserinfo',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: err,
			                    data: 'null'
			                }
						}

						res.send(result);
					}
					else
					{
						if(entity == undefined)
						{
							result = 
							{
								apiname: 'api/user/getuserinfo',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err,
				                    data: 'null'
				                }
							}

							res.send(result);
						}
						else
						{
							if(infotype.toUpperCase() == 'SUMMARY')
							{
								result = 
								{
									apiname: 'api/user/getuserinfo',
					                status: true,
					                parameters: 
					                {
					                	isAuth: true,
					                    message: 'Success',
					                    data: {
					                    	imageUrl: entity.data.imageUrl,
					                    	firstname: entity.data.firstname,
					                    	lastname: entity.data.lastname,
					                    	location: {
					                    		city: entity.data.location_city,
					                    		state: entity.data.location_state
					                    	},
					                    	requestmasjidregistration: entity.data.purchasedfeatures.createnewmasjid,
					                    	lastmodificationdate: entity.data.datemodified
					                    }
					                }
								}

								res.send(result);
							}
							else if(infotype.toUpperCase() == 'PURCHASEDMASJID')
							{
								if(entity.data.purchasedfeatures == undefined)
								{
									result = 
									{
										apiname: 'api/user/getuserinfo',
						                status: true,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: 'Success',
						                    data: {
						                    	purchasedcreatenewmasjid: false
						                    }
						                }
									}

									res.send(result);
								}
								else
								{
									result = 
									{
										apiname: 'api/user/getuserinfo',
						                status: true,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: 'Success',
						                    data: {
						                    	purchasedcreatenewmasjid: entity.data.purchasedfeatures.createnewmasjid
						                    }
						                }
									}

									res.send(result);
								}
							}
						}
					}
				});
			}	
		}
	});
});








app.post('/user/settings/edituserinfo', function(req, res){
	console.log('');
	console.log('API - Update User Info');

	// input compulsory
	var token 				= req.body.token;
	var edittype 			= req.body.edittype.toUpperCase();

	// input maybe
	var firstname 			= null;
	var lastname 			= null;
	var latitude 			= null;
	var longitude 			= null;
	var editvalue			= null;


	if(edittype == 'GENERALINFO')
	{
		firstname 			= req.body.firstname;
		lastname			= req.body.lastname;
	}
	else if(edittype == "LOCATION")
	{
		latitude 			= req.body.latitude;
		longitude 			= req.body.longitude;
	}
	else
	{
		editvalue 			= req.body.editvalue;
	}


	// output
	var result 				= {};

	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/settings/edituserinfo',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err,
                }
			}

			res.send(result);
		}
		else
		{
			var userPath = dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});
			var tx = null;
			dataset.runInTransaction(function(transaction, done){
				tx = transaction;

				transaction.get(userPath, function(err, entity){
					if(err)
					{
                        console.log('Server transaction error in updating User new information');
						tx.rollback(done);
					}
					else
					{
						if(edittype == "LOCATION")
						{
							getCityStateLocations(latitude, longitude, function(location){
									entity.data.location 			= location;
									entity.data.location_city 		= location.data.city;
									entity.data.location_state 		= location.data.state;
									entity.data.location_country 	= location.data.country;

									entity.data.datemodified = new Date();
									transaction.save(entity);
									done();
								});
						}
						else if(edittype == 'IMAGEURL')
						{
							// delete previous image before updating new image
							var defaultAvatarUrlMale = STORAGEIMAGEURL + '/images/user/USER_DEFAULT_MALE.png';
							var defaultAvatarUrlFemale = avatarUrl = STORAGEIMAGEURL + '/images/user/USER_DEFAULT_FEMALE.png';

							if(entity.data.imageUrl.avatar != defaultAvatarUrlMale && 
								entity.data.imageUrl.avatar != defaultAvatarUrlFemale)
							{
								var oldname = entity.data.imageUrl.avatar.replace(STORAGEIMAGEURL + '/', '');
								var gbucketoldimagefile = gbucket.file(oldname);
								gbucketoldimagefile.delete(function(err, apiResponse){

									var gbucketfile = gbucket.file(editvalue);
									gbucketfile.makePublic(function(err, apiResponse){
										entity.data.imageUrl.avatar = editvalue;

										entity.data.datemodified = new Date();

										transaction.save(entity);
										done();
									});
								});
							}
							else
							{
								var gbucketfile = gbucket.file(editvalue);
								gbucketfile.makePublic(function(err, apiResponse){
									entity.data.imageUrl.avatar = editvalue;
									entity.data.datemodified = new Date();
									transaction.save(entity);
									done();
								});
							}
						}
						else
						{	
							if(edittype == 'GENERALINFO')
							{
								entity.data.firstname = firstname;
								entity.data.lastname = lastname;
							}
							else if(edittype == 'PURCHASEDREGISTERNEWMASJID')
							{
								if(entity.data.purchasedfeatures == undefined)
								{
									entity.data.purchasedfeatures = {
										createnewmasjid: true,
										quotasubscribedmasjidlimit: DEFAULT_QUOTASUBSCRIBEDMASJIDLIMIT
									};

									transaction.save(entity);
								}
								else
								{
									entity.data.purchasedfeatures.createnewmasjid = true;
									transaction.save(entity);
								}
							}
							else if(edittype == 'CANCELREGISTERNEWMASJID')
							{
								entity.data.purchasedfeatures.createnewmasjid = false;

								transaction.save(entity);
							}

							entity.data.datemodified = new Date();

							transaction.save(entity);
							done();
						}
					}
				});
			}, function(err){
				if(err)
				{
					tx.rollback();

					result = 
					{
						apiname: 'api/user/settings/edituserinfo',
		                status: false,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: err,
		                }
					}

					res.send(result);
				}
				else
				{
					result = 
					{
						apiname: 'api/user/settings/edituserinfo',
		                status: true,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: 'Success',
		                }
					}

					res.send(result);
				}
			});
		}
	});
});











app.post('/user/masjid/createnewmasjid', function(req, res){
	console.log('');
	console.log('API - User registering new Masjid');


	// input
	var token 					= req.body.token;
	var masjidtype 				= req.body.masjidtype;
	var masjidname 				= req.body.newmasjidname;
	var normalizedstring 		= masjidtype + ' ' + masjidname;
	var address 				= req.body.address;
	var poscode 				= req.body.poscode;
	var city 					= req.body.city;
	var state 					= req.body.state;
	var latitude 				= req.body.latitude;
	var longitude 				= req.body.longitude;

	
	// var output
	var result 					= {};


	// internal variables
	var isLocationProvided 		= true;
	var datecreated 			= new Date();
	var datemodified 			= new Date();
	var userrole 				= 'admin';
	var tx 						= null;



	// checking before start
	if(latitude == null || longitude == null)
		isLocationProvided = false;

	normalizedstring = normalizedstring.toUpperCase(); // uppercase all characters
    normalizedstring = normalizedstring.replace(/\s+/g,""); // remove all spaces
    normalizedstring = normalizedstring.replace(/[&\/\\#,+()$~%.'":*?<>{}-]/g, '');


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/masjid/createnewmasjid',
                status: true,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			var newmasjidkey 	= dataset.key({namespace: NAMESPACE, path: [MASJIDKIND]});
			var userkey 		= dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});


			if(isLocationProvided)
			{
                console.log('Registering new Masjid with location provided');
				getCityStateLocations(latitude, longitude, function(location){
					var newmasjidapplicationdata = {
						type:               masjidtype,
						name: 				masjidname,
						nameNormalized:  	normalizedstring,
						imageUrl: 			{
												avatar: STORAGEIMAGEURLDEFAULTMASJID
											},
						location: 			location,
						location_city: 		location.data.city,
						location_state:  	location.data.state,
						location_country: 	location.data.country,
						user_input_address: address,
						user_input_city: 	city,
						user_input_state: 	state,
						user_input_poscode: poscode,
						ajk: 				[{
												userkey: 	decoded.headers.username,
												role: 		userrole
											}],
						ahlikariah: 		[{
												userkey: 	decoded.headers.username
											}],
						datecreated: 		datecreated,
						datemodified: 		datemodified
					}

					dataset.save({
						key: newmasjidkey,
						data: newmasjidapplicationdata
					}, function(err){
						if(err)
						{
                            console.log('Registration failed');
							result = 
							{
								apiname: 'api/user/masjid/createnewmasjid',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}

							res.send(result);
						}
						else
						{
                            console.log('Registration success');
                            SendEmailRegisterMasjid(decoded.headers.username, masjidname, newmasjidkey.path[1], function(){
                                
                                dataset.runInTransaction(function(transaction, done){
								tx = transaction;

                                    transaction.get(userkey, function(err, entity){
                                        if(err)
                                        {
                                            transaction.rollback(done);
                                        }
                                        else
                                        {
                                            if(entity.data.subscribedmasjids == undefined)
                                            {
                                                entity.data.subscribedmasjids = [];
                                                transaction.save(entity);
                                            }

                                            entity.data.subscribedmasjids.push({
                                                masjidKey: 			newmasjidkey.path[1],
                                                isUserGroupMasjid: 	true,
                                                role: 				userrole
                                            });

                                            entity.data.purchasedfeatures.createnewmasjid = false;

                                            entity.data.datemodified = new Date();

                                            transaction.save(entity);
                                            done();
                                        }
                                    });
                                }, function(err){
                                    if(err)
                                    {
                                        console.log('Error sending email of new masjid registration');
                                        result = 
                                        {
                                            apiname: 'api/user/masjid/createnewmasjid',
                                            status: false,
                                            parameters: 
                                            {
                                                isAuth: true,
                                                message: err
                                            }
                                        }

                                        res.send(result);
                                    }
                                    else
                                    {
                                        result = 
                                        {
                                            apiname: 'api/user/masjid/createnewmasjid',
                                            status: true,
                                            parameters: 
                                            {
                                                isAuth: true,
                                                message: 'Success'
                                            }
                                        }

                                        res.send(result);
                                    }
                                });
                            });
						}
					});

				});
			}
			else
			{
                console.log('Registration new Masjid without location provided');
				var noLocation = 
				{
				  status: false,
				  input: 
				  {
				    latitude: 'null',
				    longitude: 'null'
				  },
				  data: 
				  {
				    city: 'null',
				    state: 'null',
				    country: 'null'
				  }
				};

				var newmasjidapplicationdata = {
					type:               masjidtype,
					name: 				masjidname,
					nameNormalized:  	normalizedstring,
					imageUrl: 			{
											avatar: STORAGEIMAGEURLDEFAULTMASJID
										},
					location: 			noLocation,
					location_city: 		'null',
					location_state:  	'null',
					location_country: 	'null',
					user_input_address: address,
					user_input_city: 	city,
					user_input_state: 	state,
					user_input_poscode: poscode,
					ajk: 				[{
											userkey: 	decoded.headers.username,
											role: 		userrole
										}], 
					ahlikariah: 		[{
											userkey: 	decoded.headers.username
										}],
					datecreated: 		datecreated,
					datemodified: 		datemodified
				}

				dataset.save({
					key: newmasjidkey,
					data: newmasjidapplicationdata
				}, function(err){
					if(err)
					{
                        console.log('Registration failed');
						result = 
						{
							apiname: 'api/user/masjid/createnewmasjid',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: err
			                }
						}

						res.send(result);
					}
					else
					{
                        console.log('Registration success');
                        SendEmailRegisterMasjid(decoded.headers.username, masjidname, newmasjidkey.path[1], function(){
                            dataset.runInTransaction(function(transaction, done){
							     tx = transaction;

                                transaction.get(userkey, function(err, entity){
                                    if(err)
                                    {
                                        transaction.rollback(done);
                                    }
                                    else
                                    {
                                        if(entity.data.subscribedmasjids == undefined)
                                        {
                                            entity.data.subscribedmasjids = [];
                                            transaction.save(entity);
                                        }

                                        entity.data.subscribedmasjids.push({
                                            masjidKey: 			newmasjidkey.path[1],
                                            isUserGroupMasjid: 	true,
                                            role: 				userrole
                                        });

                                        entity.data.purchasedfeatures.createnewmasjid = false;

                                        entity.data.datemodified = new Date();

                                        transaction.save(entity);
                                        done();
                                    }
                                });
                            }, function(err){
                                if(err)
                                {
                                    console.log('Error sending email of new masjid registration');
                                    result = 
                                    {
                                        apiname: 'api/user/masjid/createnewmasjid',
                                        status: false,
                                        parameters: 
                                        {
                                            isAuth: true,
                                            message: err
                                        }
                                    }

                                    res.send(result);
                                }
                                else
                                {
                                    result = 
                                    {
                                        apiname: 'api/user/masjid/createnewmasjid',
                                        status: true,
                                        parameters: 
                                        {
                                            isAuth: true,
                                            message: 'Success'
                                        }
                                    }

                                    res.send(result);
                                }
                            });
                        });
						
					}
				});
			}
		}
	});
});













app.get('/user/search/masjid', function(req, res)
	{
		console.log('');
		console.log('API - User search masjid');

		// output
		var result 						= {};

		
		// input
		var jwtToken 					= url.parse(req.url, true).query.tokenkey;
		var searchType 					= url.parse(req.url, true).query.SearchType.toUpperCase(); 

		var searchItem 					= '';
		var userLatitude 				= 0;
		var userLongitude 				= 0;
		var maxdistant 					= 0;

		if(searchType == 'NAME')
		{
			searchItem 					= url.parse(req.url, true).query.SearchItem;
		}
		else if(searchType == 'NEAREST')
		{
			userLatitude 				= parseFloat(url.parse(req.url, true).query.userLatitude);
			userLongitude 				= parseFloat(url.parse(req.url, true).query.userLongitude);
			maxdistant 					= parseFloat(url.parse(req.url, true).query.maxdistant);
		}


		

		jwt.verify(jwtToken, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result = 
					{
						apiname: 'api/user/search/masjid',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err,
		                    data: 'null'
		                }
					}

					res.send(result);
				}
				else
				{
					if(searchType != 'NAME' 	&&
						searchType != 'NEAREST')
					{
						result = 
						{
							apiname: 'api/user/search/masjid',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: 'Unrecognized search type',
			                    data: 'null'
			                }
						}

						res.send(result);
					}
					else
					{
						if(searchType == 'NAME')
						{
							var getUserQuery = dataset.createQuery(NAMESPACE, MASJIDKIND)
							.filter('nameNormalized =', searchItem);

							dataset.runQuery(getUserQuery, function(err, entities)
							{
								if(err)
								{
                                    console.log('Server runQuery error');
									result = 
									{
										apiname: 'api/user/search/masjid',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err,
						                    data: 'null'
						                }
									}

									res.send(result);
								}
								else
								{
									if(entities.length == 0)
									{
										result = 
										{
											apiname: 'api/user/search/masjid',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                  	message: 'null',
							                  	data: 'null'
							                }
										}

										res.send(result);
									}
									else
									{
										result = 
										{
											apiname: 'api/user/search/masjid',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                  	message: 'SUCCESS',
							                  	data: entities
							                }
										}

										res.send(result);
									}
								}
							});
						}
						else if(searchType == 'NEAREST')
						{
							//var masjidpath = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND]});
							var getUserQuery = dataset.createQuery(NAMESPACE, MASJIDKIND)
								.autoPaginate(false);

							dataset.runQuery(getUserQuery, function(err, allmasjidentities){
								if(err)
								{
                                    console.log('Server runQuery Error');
									result = 
									{
										apiname: 'api/user/search/masjid',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err,
						                    data: 'null'
						                }
									}

									res.send(result);
								}
								else
								{

									var distant = -1;
									var outputdata = [];
									for (var i = 0; i < allmasjidentities.length; i++) 
									{
										//console.log(allmasjidentities[i].data.location);
										if(allmasjidentities[i].data.location.status)
										{

											distant = getDistanceFromLatLnInKm(
														userLatitude, 
														userLongitude, 
														allmasjidentities[i].data.location.input.latitude, 
														allmasjidentities[i].data.location.input.longitude);

											if(distant < maxdistant)
											{
												outputdata.push({
													id: allmasjidentities[i].key.path[1],
													name: allmasjidentities[i].data.name,
													location: {
														address: allmasjidentities[i].data.user_input_address,
														city: allmasjidentities[i].data.location_city,
														state: allmasjidentities[i].data.location_state
													},
													imageUrl: allmasjidentities[i].data.imageUrl,
													numberOfKariah: (allmasjidentities[i].data.ahlikariah == undefined? 0 : allmasjidentities[i].data.ahlikariah.length)
												});
											}
										}
									};

									if(outputdata.length > 0)
									{
										result = 
										{
											apiname: 'api/user/search/masjid',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                    message: 'SUCCESS',
							                    data: outputdata
							                }
										}

										res.send(result);

									}
									else
									{
										result = 
										{
											apiname: 'api/user/search/masjid',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                    message: 'SUCCESS',
							                    data: null
							                }
										}

										res.send(result);
									}
								}
							})
						}
					}
					
				}
			});
	});







app.post('/masjid/add/user', function(req,res)
	{
		console.log('');
		console.log('API - Masjid add user');


		// input
		var jwtToken 				= req.body.token;
		var masjidKey 				= parseInt(req.body.MasjidKey);

		// output
		var result 					= {};

		jwt.verify(jwtToken, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result =
						{
							apiname: 'api/masjid/add/user',
			                status: false,
			                parameters: 
			                {
			                	isAuth: false,
			                    message: err
			                }
						}

						res.send(result);
				}
				else
				{
					var masjidPath = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidKey]});
					var tx = null;

					dataset.runInTransaction(function(transaction, done)
					{
						tx = transaction;
						transaction.get(masjidPath, function(err, entity)
							{
								console.log(entity);
								if(err)
								{
                                    console.log('Server Transaction failed in getting masjid entity');
									transaction.rollback(done);

									result = 
									{
										apiname: 'api/masjid/add/user',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err
						                }
									}

									res.send(result);
								}
								else
								{
									if(entity.data.ahlikariah == undefined)
									{
										entity.data.ahlikariah = [];
										transaction.save(entity);
									}

									entity.data.ahlikariah.push(
										{
											userKey: decoded.headers.username
										});

									transaction.save(entity);

									done();
								}
							});
					},function(err)
					{
						if(err)
						{
							tx.rollback();
							result = 
							{
								apiname: 'api/masjid/add/user',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}
							res.send(result);
						}
						else
						{
							result = 
							{
								apiname: 'api/masjid/add/user',
				                status: true,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: "Success"
				                }
							}
							res.send(result);
						}
					});
				}
			});
	});







app.post('/user/masjid/unsubscribe', function(req,res)
	{
		console.log('');
		console.log('API - User unsubscribe a masjid');

		// input
		var jwtToken 				= req.body.token;
		var newMasjidId 			= parseInt(req.body.MasjidKey);
		var indexLookup 			= -1;


		// output
		var result 					= {};


		jwt.verify(jwtToken, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result = 
					{
						apiname: 'api/user/unsubscribe/masjid',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					var userPath = dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});
					var masjidPath = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, newMasjidId]});
					var tx = null;

					dataset.runInTransaction(function(transaction, done)
						{
							tx = transaction;
							transaction.get(userPath, function(err, entity)
								{
									if(err)
									{
                                        console.log('Server Transaction Error during getting User entity');
										transaction.rollback(done);

										result = 
										{
											apiname: 'api/user/subscribe/masjid',
							                status: false,
							                parameters: 
							                {
							                	isAuth: true,
							                    message: err
							                }
										}

										res.send(result);
									}
									else
									{
										// remove from user
										if(entity.data.subscribedmasjids != undefined)
										{
											if(entity.data.subscribedmasjids.length > 0)
											{
												// find the masjid
												for (var i = 0; i < entity.data.subscribedmasjids.length; i++) 
												{
													if(entity.data.subscribedmasjids[i].masjidKey == newMasjidId)
													{
														indexLookup = i;
														break;
													}
												};

												if(indexLookup != -1)
												{
													entity.data.subscribedmasjids.splice(indexLookup, 1);
													transaction.save(entity);
												}
											}
										}

										indexLookup = -1;

										// remove user from masjid
										transaction.get(masjidPath, function(err, masjidEntity)
											{
												if(err)
												{
                                                    console.log('Server transaction error during getting Masjid entity');
													transaction.rollback(done);

													result = 
													{
														apiname: 'api/user/subscribe/masjid',
										                status: false,
										                parameters: 
										                {
										                	isAuth: true,
										                    message: err
										                }
													}

													res.send(result);
												}
												else
												{
													if(masjidEntity.data.ahlikariah != undefined)
													{
														if(masjidEntity.data.ahlikariah.length > 0)
														{
															for (var i = 0; i < masjidEntity.data.ahlikariah.length; i++) 
															{
																if(masjidEntity.data.ahlikariah[i].userkey == decoded.headers.username)
																{
																	indexLookup = i;
																	break;
																}
															};

															if(indexLookup > -1)
															{
																masjidEntity.data.ahlikariah.splice(indexLookup, 1);
																transaction.save(masjidEntity);
															}
														}
													}
												}

												done();
											});

									}
								});
						}, function(err)
						{
							if(err)
							{
								result = 
								{
									apiname: 'api/user/unsubscribe/masjid',
					                status: false,
					                parameters: 
					                {
					                	isAuth: true,
					                    message: err
					                }
								}

								res.send(result);
							}
							else
							{
								result = 
								{
									apiname: 'api/user/unsubscribe/masjid',
					                status: true,
					                parameters: 
					                {
					                	isAuth: true,
					                    message: "Success"
					                }
								}

								res.send(result);
							}
						});
				}
			});
	});










app.post('/user/subsribe/masjid', function(req, res)
	{
		console.log('');
		console.log('API - User subscribe a masjid');


		// input
		var jwtToken 				= req.body.token;
		var newMasjidId 			= parseInt(req.body.MasjidKey);

		// output
		var result 					= {};


		jwt.verify(jwtToken, app.get('passwordsecret'), function(err, decoded)
		{
			if(err)
			{
                console.log('Authentication failed');
				result = 
					{
						apiname: 'api/user/subscribe/masjid',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err
		                }
					}

					res.send(result);
			}
			else
			{
				var userPath = dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});
				var masjidPath = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, newMasjidId]});
				var tx = null;
				
				dataset.runInTransaction(function(transaction, done)
					{
						tx = transaction;
						transaction.get(userPath, function(err, entity)
							{
								if(err)
								{
                                    console.log('Server Transactio error during getting User entity');
									transaction.rollback(done);

									result = 
									{
										apiname: 'api/user/subscribe/masjid',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err
						                }
									}

									res.send(result);
								}
								else
								{
									if(entity.data.subscribedmasjids == undefined)
									{
										// create the new object
										entity.data.subscribedmasjids = [];
										transaction.save(entity);
									}
									
									// add new masjid
									entity.data.subscribedmasjids.push({
										masjidKey: newMasjidId
									});
									transaction.save(entity);


									// transaction masjid to add user
									transaction.get(masjidPath, function(err, masjidEntity)
										{
											if(err)
											{
                                                console.log('Server Transaction Error during getting Masjid entity');
												transaction.rollback(done);

												result = 
												{
													apiname: 'api/user/subscribe/masjid',
									                status: false,
									                parameters: 
									                {
									                	isAuth: true,
									                    message: err
									                }
												}

												res.send(result);
											}
											else
											{
												if(masjidEntity.data.ahlikariah == undefined 	||
													masjidEntity.data.ahlikariah == null)
												{
													masjidEntity.data.ahlikariah = [];
													transaction.save(masjidEntity);
												}

												masjidEntity.data.ahlikariah.push(
													{
														userkey: decoded.headers.username
													});

												transaction.save(masjidEntity);

												done();
											}
										});
								}
							});

					}, function(err)
					{
						if(err)
						{
							tx.rollback();
							result = 
							{
								apiname: 'api/user/subscribe/masjid',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}

							res.send(result);
						}
						else
						{
							result = 
							{
								apiname: 'api/user/subscribe/masjid',
				                status: true,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: "Success"
				                }
							}

							res.send(result);
						}
					});
			}
		});
	});







app.get('/user/masjid/getallmasjid', function(req, res){
	console.log('');
	console.log('API - User get all registered masjid')


	//input
	var token 				= url.parse(req.url, true).query.token;


	//output
	var result 				= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/get/allmasjid',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			var getallmasjidquery = dataset.createQuery(NAMESPACE, MASJIDKIND)
				// .select(['ahlikariah', 'imageUrl', 'user_input_address', 'location_city', 'location_state', 'name'])
				.offset(0)
				.autoPaginate(false);

				var outputdata = [];

			var queryCallback = function(err, masjidentities, nextQuery, apiResponse)
			{
				if(err)
				{
                    console.log('Server query Error in getting all Masjid entity');
					if(outputdata.length > 0)
					{
                        console.log('Few Masjid entity successfully retrieve during previous query. Sending to requestor');
						result = 
						{
							apiname: 'api/user/get/allmasjid',
			                status: true,
			                parameters: 
			                {
			                	isAuth: true,
			                    data: outputdata
			                }
						}

						res.send(result);
					}
					else
					{
						result = 
						{
							apiname: 'api/user/get/allmasjid',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: err
			                }
						}

						res.send(result);
					}
				}
				else
				{
					for (var i = 0; i < masjidentities.length; i++) {
						outputdata.push({
							id: masjidentities[i].key.path[1],
							name: masjidentities[i].data.name,
							imageUrl: masjidentities[i].data.imageUrl,
							location_city: masjidentities[i].data.location_city,
							location_state: masjidentities[i].data.location_state,
							address: masjidentities[i].data.user_input_address,
							ahlikariah: masjidentities[i].data.ahlikariah.length
						});
						// outputdata.push(masjidentities[i]);
					};

					if(nextQuery)
					{
                        console.log('Check if more data is available');
						dataset.runQuery(nextQuery, queryCallback);
					}
					else
					{
						result = 
						{
							apiname: 'api/user/get/allmasjid',
			                status: true,
			                parameters: 
			                {
			                	isAuth: true,
			                    data: outputdata
			                }
						}

						res.send(result);
					}
				}
			}

			dataset.runQuery(getallmasjidquery, queryCallback);
		}
	});
});









app.get('/user/masjid/getmasjidinfo', function(req, res)
	{
		console.log('');
		console.log('API - User get masjid data');

		// input
		var token 					= url.parse(req.url, true).query.token;
		var searchtype 				= url.parse(req.url, true).query.searchtype.toUpperCase();
		var masjidkey 				= parseInt(url.parse(req.url, true).query.masjidkey);

		// output
		var result 					= {};


		jwt.verify(token, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result = 
					{
						apiname: 'api/user/get/masjidinfo',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					if(searchtype != 'DETAILS' 			&&
						searchtype != 'SUMMARY'			&&
						searchtype != "CARDSHOWCASE"	&&
						searchtype != 'SETTINGS'		&&
						searchtype != 'SUBSCRIBER')
					{
						result = 
						{
							apiname: 'api/user/get/masjidinfo',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: "Search type is not recognized."
			                }
						}

						res.send(result);
					}
					else
					{
						var masjidPath = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidkey]});
						dataset.get(masjidPath, function(err, entity)
							{
								if(err)
								{
                                    console.log('Server Error during getting Masjid entity');
									result = 
									{
										apiname: 'api/user/get/masjidinfo',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err
						                }
									}

									res.send(result);
								}
								else
								{
									if(entity != undefined)
									{
										if(searchtype == 'DETAILS')
										{
											result = 
											{
												apiname: 'api/user/get/masjidinfo',
								                status: true,
								                parameters: 
								                {
								                	isAuth: true,
								                    data: entity
								                }
											}

											res.send(result);
										}
										else if(searchtype == "CARDSHOWCASE")
										{
											var isUserCanEdit = false;
											var isUserSubscribed = false;

											// check if user can edit
											for (var i = 0; i < entity.data.ajk.length; i++) {
												if(entity.data.ajk[i].userkey == decoded.headers.username)
												{
													if(entity.data.ajk[i].role.toUpperCase() == 'ADMIN')
													{
														isUserCanEdit = true;
														break;
													}
												}
											};

											// check if user already subscribe
											for (var i = 0; i < entity.data.ahlikariah.length; i++) {
												if(entity.data.ahlikariah[i].userkey == decoded.headers.username)
												{
													isUserSubscribed = true;
													break;
												}
											};

											result = 
											{
												apiname: 'api/user/get/masjidinfo',
								                status: true,
								                parameters: 
								                {
								                	isAuth: true,
								                    data: {
								                    	id: entity.key.path[1],
								                    	name: entity.data.name,
								                    	location: entity.data.location,
								                    	imageUrl: entity.data.imageUrl,
								                    	numberOfKariah: (entity.data.ahlikariah == undefined ? 0 : entity.data.ahlikariah.length),
								                    	canedit: isUserCanEdit,
								                    	issubscribed: isUserSubscribed
								                    }
								                }
											}

											res.send(result);
										}
										else if(searchtype == 'SETTINGS')
										{
											var userskey = [];
											for (var i = 0; i < entity.data.ajk.length; i++) {
												userskey.push(dataset.key({namespace: NAMESPACE, path: [USERKIND, entity.data.ajk[i].userkey]}));
											};

											dataset.get(userskey, function(err, entities){
												if(err)
												{
													result = 
													{
														apiname: 'api/user/get/masjidinfo',
										                status: true,
										                parameters: 
										                {
										                	isAuth: true,
										                    data: {
										                    	id: entity.key.path[1],
										                    	name: entity.data.name,
										                    	location: entity.data.location,
										                    	imageUrl: entity.data.imageUrl,
										                    	ajk: null,
										                    	datemodified: entity.data.datemodified
										                    }
										                }
													}

													res.send(result);
												}
												else
												{
													var ajkdetails = [];
													var ajkrole = '';



													for (var i = 0; i < entities.length; i++) {

														for (var j = 0; j < entity.data.ajk.length; j++) {
															if(entity.data.ajk[j].userkey == entities[i].key.path[1])
															{
																ajkrole = entity.data.ajk[j].role
																break;
															}
														};

														ajkdetails.push({
															firstname: entities[i].data.firstname,
															lastname: entities[i].data.lastname,
															imageUrl: entities[i].data.imageUrl.avatar,
															userkey: entities[i].key.path[1],
															role: ajkrole
														}) 
													};

													result = 
													{
														apiname: 'api/user/get/masjidinfo',
										                status: true,
										                parameters: 
										                {
										                	isAuth: true,
										                    data: {
										                    	id: entity.key.path[1],
										                    	name: entity.data.name,
										                    	location: entity.data.location,
										                    	imageUrl: entity.data.imageUrl,
										                    	ajk: ajkdetails,
										                    	datemodified: entity.data.datemodified
										                    }
										                }
													}

													res.send(result);
												}
											});

											
										}
										else if(searchtype == 'SUBSCRIBER')
										{
											var ahlikariahkey = [];
											var outputdata = [];
											for (var i = 0; i < entity.data.ahlikariah.length; i++) {

												for(var j = 0; j < entity.data.ajk.length; j++)
												{
													if(entity.data.ahlikariah[i].userkey != entity.data.ajk[j].userkey)
													{
														ahlikariahkey.push(dataset.key({namespace: NAMESPACE, path: [USERKIND, entity.data.ahlikariah[i].userkey]}))
													}
												}
											};

											if(ahlikariahkey.length > 0)
											{
												dataset.get(ahlikariahkey, function(err, userentities){
													if(err)
													{
														result = 
														{
															apiname: 'api/user/get/masjidinfo',
											                status: false,
											                parameters: 
											                {
											                	isAuth: true,
											                    message: err
											                }
														}

														res.send(result);
													}
													else
													{
														for (var i = 0; i < userentities.length; i++) {
															outputdata.push({
																id: userentities[i].key.path[1],
																firstname: userentities[i].data.firstname,
																lastname: userentities[i].data.lastname,
																imageurl: userentities[i].data.imageUrl
															});
															
														};

														result = 
														{
															apiname: 'api/user/get/masjidinfo',
											                status: true,
											                parameters: 
											                {
											                	isAuth: true,
											                    data: outputdata
											                }
														}

														res.send(result);
													}
												});
											}
											else
											{
												result = 
												{
													apiname:  'api/user/get/masjidinfo',
									                status: true,
									                parameters: 
									                {
									                	isAuth: true,
									                    data: null
									                }
												}

												res.send(result);
											}
										}
										else
										{
											result = 
											{
												apiname:  'api/user/get/masjidinfo',
								                status: true,
								                parameters: 
								                {
								                	isAuth: true,
								                    data: null
								                }
											}

											res.send(result);
										}
									}
									else
									{
										result = 
										{
											apiname: 'api/user/subscribe/masjid',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                    data: null
							                }
										}

										res.send(result);
									}
								}
							});
					}
				}
			});

	});





app.post('/user/masjid/editmasjidinfo', function(req, res){
	console.log('');
	console.log('API - User edit Masjid Info');


	// input
	var token 				= req.body.token;
	var masjidid 			= parseInt(req.body.masjidid);
	var edittype 			= req.body.edittype.toUpperCase();


	// var maybe
	var latitude 			= null;
	var longitude 			= null;
	var editvalue 			= null;


	if(edittype == 'LOCATIONS')
	{
		latitude 			= req.body.latitude;
		longitude 			= req.body.longitude;
	}
	else
	{
		editvalue 			= req.body.editvalue;
	}


	// output
	var result 				= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/masjid/editmasjidinfo',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			if(edittype != 'LOCATIONS' 	&&
				edittype != 'NAME' 		&&
				edittype != 'IMAGEURL'  &&
				edittype != 'ADDAJK' 	&&
				edittype != 'REMOVEAJK' &&
				edittype != 'REMOVESELFFROMAJK')
			{
				result = 
				{
					apiname: 'api/user/masjid/editmasjidinfo',
	                status: false,
	                parameters: 
	                {
	                	isAuth: true,
	                    message: 'Unrecognize edit type info'
	                }
				}

				res.send(result);
			}
			else
			{
				var masjidkey = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidid]});
				var tx = null;

				dataset.runInTransaction(function(transaction, done){
					tx = transaction;

					transaction.get(masjidkey, function(err, entity){
						if(err)
						{
                            console.log('Server Transaction Error during getting masjid entity');
							transaction.rollback(done);
						}
						else
						{
							if(edittype == 'LOCATIONS')
							{
								getCityStateLocations(latitude, longitude, function(location){
									entity.data.location 			= location;
									entity.data.location_city 		= location.data.city;
									entity.data.location_state 		= location.data.state;
									entity.data.location_country 	= location.data.country;

									entity.data.datemodified = new Date();
									transaction.save(entity);

									done();
								});
							}
							else if(edittype == 'IMAGEURL')
							{
								if(entity.data.imageUrl.avatar != STORAGEIMAGEURLDEFAULTMASJID)
								{
									var oldname = entity.data.imageUrl.avatar.replace(STORAGEIMAGEURL + '/', '');
									var gbucketoldimagefile = gbucket.file(oldname);
									gbucketoldimagefile.delete(function(err, apiResponse){

										var gbucketfile = gbucket.file(editvalue);
										gbucketfile.makePublic(function(err, apiResponse){
											entity.data.imageUrl.avatar = editvalue;

											entity.data.datemodified = new Date();

											transaction.save(entity);
											done();
										});
									});
								}
								else
								{
									var gbucketfile = gbucket.file(editvalue);
									gbucketfile.makePublic(function(err, apiResponse){
										entity.data.imageUrl.avatar = editvalue;

										entity.data.datemodified = new Date();

										transaction.save(entity);
										done();
									});
								}
							}
							else
							{
								if(edittype == 'NAME')
								{
									var normalizedstring = editvalue;
									normalizedstring = normalizedstring.toUpperCase(); // uppercase all characters
								    normalizedstring = normalizedstring.replace(/\s+/g,""); // remove all spaces
								    normalizedstring = normalizedstring.replace(/[&\/\\#,+()$~%.'":*?<>{}-]/g, '');

									entity.data.name = editvalue;
									entity.data.nameNormalized = normalizedstring;
								}
								else if(edittype == 'ADDAJK')
								{
									entity.data.ajk.push({
										role: 'admin',
										userkey: editvalue
									})
								}
								else if(edittype == 'REMOVEAJK')
								{
									var indexlookup = -1
									for (var i = 0; i < entity.data.ajk.length; i++) {
										if(entity.data.ajk[i].userkey == editvalue)
										{
											indexlookup = i;
											break;
										}
									};

									if(indexlookup > -1)
									{
										entity.data.ajk.splice(indexlookup, 1);
									}
								}
								else if(edittype == 'REMOVESELFFROMAJK')       
								{
									var indexlookup = -1;

									for (var i = 0; i < entity.data.ajk.length; i++) {
										if(entity.data.ajk[i].userkey == decoded.headers.username)
										{
											indexlookup = i;
											break;
										}
									};

									if(indexlookup > -1)
									{
										entity.data.ajk.splice(indexlookup, 1);
									}
								}
								

								entity.data.datemodified = new Date();
								transaction.save(entity);

								done();
							}

							
						}
					});

				}, function(err){
					if(err)
					{
						tx.rollback();

						result = 
						{
							apiname: 'api/user/masjid/editmasjidinfo',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: err
			                }
						}

						res.send(result);
					}
					else
					{
						result = 
						{
							apiname: 'api/user/masjid/editmasjidinfo',
			                status: true,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: 'Success'
			                }
						}

						res.send(result);
					}
				});
			}
		}
	});
});









app.get('/user/get/subscribedmasjid', function(req, res)
	{
		console.log('');
		console.log('API - User get subsribed masjid');

		// input
		var token 						= url.parse(req.url, true).query.token;
		var searchtype 					= url.parse(req.url, true).query.searchtype != 'details' ? 'summary' : 'details';


		// output
		var result 						= {};


		jwt.verify(token, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result = 
					{
						apiname: 'api/user/subscribe/masjid',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					var userPath = dataset.key({namespace: NAMESPACE, path: [USERKIND, decoded.headers.username]});
					dataset.get(userPath, function(err, entity)
						{
							if(err)
							{
                                console.log('Server error during getting User entity');
								result = 
								{
									apiname: 'api/user/subscribe/masjid',
					                status: false,
					                parameters: 
					                {
					                	isAuth: true,
					                    message: err
					                }
								}

								res.send(result);	
							}
							else
							{
								if(entity.data.subscribedmasjids == undefined	|| 
									entity.data.subscribedmasjids == null		||
									entity.data.subscribedmasjids == [])
								{
									result = 
									{
										apiname: 'api/user/subscribe/masjid',
						                status: true, 
						                parameters: 
						                {
						                	isAuth: true,
						                    data: null
						                }
									}

									res.send(result);
								}
								else
								{
									if(entity.data.subscribedmasjids.length > 0)
									{
										var listMasjidPath = [];
										for (var i = 0; i < entity.data.subscribedmasjids.length; i++) 
										{
											listMasjidPath.push(
												dataset.key({namespace: NAMESPACE, path: [
													MASJIDKIND, parseInt(entity.data.subscribedmasjids[i].masjidKey)
													]})
												);
											
										};

										dataset.get(listMasjidPath, function(err, masjidEntities)
											{
												if(err)
												{
                                                    console.log('Server error during getting Masjid entities');
													result = 
													{
														apiname: 'api/user/subscribe/masjid',
										                status: false,
										                parameters: 
										                {
										                	isAuth: true,
										                    message: err
										                }
													}

													res.send(result);
												}
												else
												{
													if(searchtype == "details")
													{
														result = 
														{
															apiname: 'api/user/subscribe/masjid',
											                status: true,
											                parameters: 
											                {
											                	isAuth: true,
											                    data: masjidEntities
											                }
														}
													}
													else
													{
														var outputdetails = [];
														for (var i = 0; i < masjidEntities.length; i++) 
														{
															outputdetails.push(
																{
																	id: masjidEntities[i].key.path[1],
																	name: masjidEntities[i].data.name,
																	location: {
																		address: masjidEntities[i].data.user_input_address,
																		city: masjidEntities[i].data.location_city,
																		state: masjidEntities[i].data.location_state
																	},
																	imageUrl: masjidEntities[i].data.imageUrl,
																	numberOfKariah: (masjidEntities[i].data.ahlikariah == undefined? 0 : masjidEntities[i].data.ahlikariah.length)
																});
														};

														result = 
														{
															apiname: 'api/user/subscribe/masjid',
											                status: true,
											                parameters: 
											                {
											                	isAuth: true,
											                    data: outputdetails
											                }
														}
													}

													res.send(result);
												}
											});
									}
									else
									{
										result = 
										{
											apiname: 'api/user/subscribe/masjid',
							                status: true, 
							                parameters: 
							                {
							                	isAuth: true,
							                    data: null
							                }
										}

										res.send(result);
									}
									
								}
							}
						});
				}
			});
	});













app.post('/user/masjid/addceramah', function(req, res)
	{
        console.log('');
        console.log('API - New Activity Registration');
    
		// input
		var token 			= req.body.token;
		var masjidkey 		= parseInt(req.body.masjidkey); 	// masjid key
		var imageUrl 		= req.body.imageurl; 				// program picture
		var activitytype 	= req.body.activitytype;			// ceramah, kelas, kursus, etc
		var title 			= req.body.title;					// tajuk
		var bidang 			= req.body.bidang;					// fiqh, usuluddin, tajwid, etc
		var guest 			= req.body.guest;					// penceramah, tuan/puan guru
		var venue			= req.body.venue;
		var startdatetime	= new Date(req.body.startdatetime);	// start date
		var repeat			= req.body.repeat;					// is ceramah repeat?
		var enddate			= new Date(req.body.enddate);		// end date if the ceramah is repeated
		var frequency		= req.body.frequency.toUpperCase();	// frequency

		var guestJSON = [];
		for (var i = 0; i < guest.length; i++) 
		{
			guestJSON.push(guest[i]);
		};


		// output
		var result = {};

		

		jwt.verify(token, app.get('passwordsecret'), function(err, decoded)
			{
				if(err)
				{
                    console.log('Authentication failed');
					result = 
					{
						apiname: 'api/masjid/addceramah',
		                status: false,
		                parameters: 
		                {
		                	isAuth: false,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					var masjidProgramKey = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidkey, ACTIVITYKIND]});
					//var aktivityPath = dataset.key(ACTIVITYKIND);
					var aktivityData = {
						// location
						// city: masjidEntity.data.location_city,
						// state: masjidEntity.data.location_state,
						// location: location,
						// isAktivityMasjid: true,
						// masjidId: masjidEntity.key.path[1],
						// venue: masjidEntity.data.name,
						additionalvenue: venue,

						// info
						imageUrl: imageUrl,
						type: activitytype,
						knowledgebase: bidang,
						title: title,
						guest: guestJSON,
						status: true,

						// datetime
						startdate: startdatetime,
						isRepeat: repeat,
						enddate: enddate,
						frequency: frequency,
						datecreated: new Date(),
						datemodified: new Date()
					}

					dataset.save({
						key: masjidProgramKey,
						data: aktivityData
					}, function(err){
						if(err)
						{
                            console.log('Registration failed');
							result = 
							{
								apiname: 'api/masjid/addceramah',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}

							res.send(result);
						}
						else
						{
                            console.log('Registration success');
							result = 
							{
								apiname: 'api/masjid/addceramah',
				                status: true,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: 'Success'
				                }
							}

							res.send(result);
						}
					});
				}
			});

	});








app.get('/user/masjid/getceramah', function(req, res){

	console.log('');
	console.log('API - User get Masjid Program');

	// input
	var token 			= url.parse(req.url, true).query.token;
	var searchtype 		= url.parse(req.url, true).query.searchtype.toUpperCase();
	var masjidid 		= parseInt(url.parse(req.url, true).query.masjidid);
	var programid 		= -1;
	var beforedate 		= new Date();
	

	if(searchtype == 'PROGRAMID')
	{
		programid 		= parseInt(url.parse(req.url, true).query.programid);
	}

	// output
	var result 			= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/masjid/getceramah',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			if(searchtype != "ALL" 		&& 
				searchtype != "BEFOREDATE" 	&&
				searchtype != 'PROGRAMID')
			{
				result = 
				{
					apiname: 'api/user/masjid/getceramah',
	                status: false,
	                parameters: 
	                {
	                	isAuth: true,
	                    message: "Unrecognized search type."
	                }
				}

				res.send(result);
			}
			else
			{
				if(searchtype == 'PROGRAMID')
				{
					
					var programkey = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidid, ACTIVITYKIND, programid]});
					dataset.get(programkey, function(err, programentity){
						if(err)
						{
                            console.log('Server error during getting activity entity');
							result = 
							{
								apiname: 'api/user/masjid/getceramah',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}

							res.send(result);
						}
						else
						{
							result = 
							{
								apiname: 'api/user/masjid/getceramah',
				                status: true,
				                parameters: 
				                {
				                	isAuth: true,
				                    data: programentity
				                }
							}

							res.send(result);
						}
					});
				}
				else
				{
					var masjidPaths = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidid]});

					dataset.get(masjidPaths, function(err, entities)
					{
						if(err)
						{
                            console.log('Server error during getting masjid entity');
							result = 
							{
								apiname: 'api/user/masjid/getceramah',
				                status: false,
				                parameters: 
				                {
				                	isAuth: true,
				                    message: err
				                }
							}

							res.send(result);
						}
						else
						{
							var masjidInfo = {};
							var outputdata = [];
							var queryTamanSyurga = null;

							masjidInfo = entities;

							queryTamanSyurga = dataset.createQuery(NAMESPACE, ACTIVITYKIND)
										.hasAncestor(dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidInfo.key.path[1]]}))
										.autoPaginate(false);


							
							function queryCallback(err, programEntities)
							{
								if(err)
								{
                                    console.log('Server error during getting activity entities');
									result = 
									{
										apiname: 'api/user/masjid/getceramah',
						                status: false,
						                parameters: 
						                {
						                	isAuth: true,
						                    message: err
						                }
									}

									res.send(result);
								}
								else
								{
									if(programEntities.length > 0)
									{
										for (var k = 0; k < programEntities.length; k++) {
											if(searchtype == 'BEFOREDATE')
											{
												if(programEntities[k].data.enddate > beforedate)
												{
													outputdata.push({
														masjidinfo: {
															id: masjidInfo.key.path[1],
															name: masjidInfo.data.name,
															location: masjidInfo.data.location,
															imageUrl: masjidInfo.data.imageUrl
														},
														programinfo: {
															id: programEntities[k].key.path[3],
															info: programEntities[k].data
														}
													});
												}
											}
											else if(searchtype == 'ALL')
											{
												outputdata.push({
													masjidinfo: {
														id: masjidInfo.key.path[1],
														name: masjidInfo.data.name,
														location: masjidInfo.data.location,
														imageUrl: masjidInfo.data.imageUrl
													},
													programinfo: {
														id: programEntities[k].key.path[3],
														info: programEntities[k].data
													}
												});
											}
										};

										result = 
										{
											apiname: 'api/user/masjid/getceramah',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                    data: outputdata
							                }
										}

										res.send(result);

									}
									else
									{
										result = 
										{
											apiname: 'api/user/masjid/getceramah',
							                status: true,
							                parameters: 
							                {
							                	isAuth: true,
							                    data: null
							                }
										}

										res.send(result);
									}
								}
							};

							

							dataset.runQuery(queryTamanSyurga, queryCallback);
						}
					});
				}
				
			}
		}
	});
});







app.post('/user/masjid/editceramah', function(req, res){
	console.log('');
	console.log('API - User edit ceramah');


	// input
	var token 				= req.body.token;
	var masjidid 			= parseInt(req.body.masjidid);
	var programid 			= parseInt(req.body.programid);
	var edittype 			= req.body.edittype.toUpperCase();
	var editvalue 			= null;

	if(edittype == 'STARTDATE' 	||
		edittype == 'ENDDATE')
	{
		editvalue 			= new Date(req.body.editvalue);
	}
	else
	{
		editvalue 			= req.body.editvalue;
	}

	var result 				= {};

	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/masjid/editceramah',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			if(edittype != 'STATUS'				&&
				edittype != 'TITLE'				&&
				edittype != 'ADDITIONALVENUE'	&&
				edittype != 'ISREPEAT'			&&
				edittype != 'STARTDATE'			&&
				edittype != 'ENDDATE'			&&
				edittype != 'FREQUENCY' 		&&
				edittype != 'ADDGUEST'			&&
				edittype != 'IMAGEURL'			&&
				edittype != 'REMOVEGUEST' 		&&
				edittype != 'IMAGEURL')
			{
				result = 
				{
					apiname: 'api/user/masjid/editceramah',
	                status: false,
	                parameters: 
	                {
	                	isAuth: true,
	                    message: 'Edit type is not recognized'
	                }
				}

				res.send(result);
			}
			else
			{
				var programkey = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidid, ACTIVITYKIND, programid]});
				var tx = null

				dataset.runInTransaction(function(transaction, done){
					tx = transaction;

					transaction.get(programkey, function(err, programentity){
						if(err)
						{
                            console.log('Server Transaction error during getting activity entity');
							transaction.rollback(done);
						}
						else
						{
							if(edittype == 'IMAGEURL')
							{
								if(programentity.data.imageUrl != STORAGEIMAGEURLDEFAULTPROGRAM)
								{
									var oldname = programentity.data.imageUrl.replace(STORAGEIMAGEURL + '/', '');
									var gbucketoldimagefile = gbucket.file(oldname);
									gbucketoldimagefile.delete(function(err, apiResponse){

										var gbucketfile = gbucket.file(editvalue);
										gbucketfile.makePublic(function(err, apiResponse){
											programentity.data.imageUrl = editvalue;

											programentity.data.datemodified = new Date();

											transaction.save(programentity);
											done();
										});
									});
								}
								else
								{
									var gbucketfile = gbucket.file(editvalue);
									gbucketfile.makePublic(function(err, apiResponse){
										programentity.data.imageUrl = editvalue;

										programentity.data.datemodified = new Date();

										transaction.save(programentity);
										done();
									});
								}
							}
							else
							{
								if(edittype == 'STATUS')
								{
									programentity.data.status = editvalue;
								}
								else if(edittype == 'TITLE')
								{
									programentity.data.title = editvalue;
								}
								else if(edittype == 'ADDITIONALVENUE')
								{
									programentity.data.additionalvenue = editvalue;
								}
								else if(edittype == 'ISREPEAT')
								{
									programentity.data.isRepeat = editvalue;
								}
								else if(edittype == 'STARTDATE')
								{
									programentity.data.startdate = editvalue;
								}
								else if(edittype == 'ENDDATE')
								{
									programentity.data.enddate = editvalue;
								}
								else if(edittype == 'FREQUENCY')
								{
									programentity.data.frequency = editvalue;
								}
								else if(edittype == 'ADDGUEST')
								{
									programentity.data.guest.push({
							  			name: editvalue,
							  			isGuestAppsUser: false,
							  			data: {
							  				nodata: true
							  			} // if isGuestAppsUser == true, then data will hold general info of the guest (avatar, name, etc)
							  		});
								}
								else if(edittype == 'REMOVEGUEST')
								{
									var lookupindex = -1;
									for (var i = 0; i < programentity.data.guest.length; i++) {
										if(programentity.data.guest[i].name == editvalue)
										{
											lookupindex = i;
											break;
										}
									};

									if(lookupindex > -1)
									{
										programentity.data.guest.splice(lookupindex, 1);
									}
								}

								programentity.data.datemodified = new Date();
								transaction.save(programentity);
								done();
							}
						}
					});
				}, function(err){
					if(err)
					{
						result = 
						{
							apiname: 'api/user/masjid/editceramah',
			                status: false,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: err
			                }
						}

						res.send(result);
					}
					else
					{
						result = 
						{
							apiname: 'api/user/masjid/editceramah',
			                status: true,
			                parameters: 
			                {
			                	isAuth: true,
			                    message: 'SUCCESS'
			                }
						}

						res.send(result);
					}
				});

			}
		}
	});
});








app.post('/user/masjid/deleteceramah', function(req, res){
    
    console.log('');
    console.log('API - User delete activity');
    
	// input
	var token 				= req.body.token;
	var ceramahid			= parseInt(req.body.ceramahid);
	var masjidId 			= parseInt(req.body.masjidid);


	// output
	var result 				= {};


	jwt.verify(token, app.get('passwordsecret'), function(err, decoded){
		if(err)
		{
            console.log('Authentication failed');
			result = 
			{
				apiname: 'api/user/masjid/deleteceramah',
                status: false,
                parameters: 
                {
                	isAuth: false,
                    message: err
                }
			}

			res.send(result);
		}
		else
		{
			// delete from Masjid entity
			var masjidKey = dataset.key({namespace: NAMESPACE, path: [MASJIDKIND, masjidId]});
			var tx = null;
			dataset.runInTransaction(function(transaction, done){
				tx = transaction;

				transaction.get(masjidKey, function(err, entity){
					if(err)
					{
                        console.log('Server transaction Error during getting Masjid entity');
						transaction.rollback(done);
					}
					else
					{
						var index = -1;
						for (var i = 0; i < entity.data.activity.length; i++) {
							if(entity.data.activity[i].activityId == ceramahid)
							{
								index = i;
								break;
							}
						};

						// delete data
						entity.data.activity.splice(index, 1);
						transaction.save(entity);


						// delete from kind group
						var ceramahPath = dataset.key({namespace: NAMESPACE, path: [ACTIVITYKIND, ceramahid]});
						dataset.delete(ceramahPath, function(err, apiResponse){
							if(err)
							{
								tx.rollback(done);
							}
							else
							{
								done();
							}
						});
					}
				});
			}, function(err){
				if(err)
				{
					tx.rollback();

					result = 
					{
						apiname: 'api/user/masjid/deleteceramah',
		                status: false,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: err
		                }
					}

					res.send(result);
				}
				else
				{
					result = 
					{
						apiname: 'api/user/masjid/deleteceramah',
		                status: true,
		                parameters: 
		                {
		                	isAuth: true,
		                    message: "Success"
		                }
					}

					res.send(result);
				}
			});
		}
	});
});












//
// START THE SERVER
//
app.listen(ServerPort);
console.log('Welcome to Jom Pi Masjid Api Server');
console.log('Powered by PoPoi Menenet');
console.log('');
console.log('Server starts at port '+ServerPort);






















